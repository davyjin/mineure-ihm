package com.ihm.starquizz;

import android.app.Activity;
import android.os.Bundle;

public class Configuration extends Activity {

	public static final String MAL_VOYANT_PREF = "prefMalVoyant";
	public static final String MAL_ENTENDANT_PREF = "prefMalEntendant";
	public static final String VOICE_INPUT_PREF = "prefVoiceInput";
	public static final String VIBREUR_PREF = "prefVibreur";

	
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);	    
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
	}

}
