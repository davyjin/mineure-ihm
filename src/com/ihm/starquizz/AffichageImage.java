package com.ihm.starquizz;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class AffichageImage extends Activity {

	ImageViewTouch image;
	Bitmap bitmap;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_affichage);
		this.image = (ImageViewTouch) findViewById(R.id.imageFullScreen);		
	}
	
	@Override
	public void onContentChanged() {
		super.onContentChanged();
	}
	
	@Override
	protected void onResume() {
		super.onStart();
		Intent data = getIntent();
		byte[] bytes = data.getByteArrayExtra("BMP");
        this.bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        this.image.setDisplayType(DisplayType.FIT_TO_SCREEN);
		this.image.setImageBitmap(this.bitmap);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
}
