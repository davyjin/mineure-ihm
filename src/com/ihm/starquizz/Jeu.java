package com.ihm.starquizz;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.util.SparseIntArray;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class Jeu extends Activity implements TextToSpeech.OnInitListener {
	private final static int VOICE_INPUT_SUBACTIVITY = 1;
	private static final int MAX_JOKER = 3;	//Nombre maximal de jokers
	private ImageView imageView;
	private int indexImg = 0;	//index de l'image courant dans la liste de résultats
	private PhotoList photoListe = null;	//liste de photos, résultat de la recherche
	private Toast mToast;	//Toast permettant d'afficher des messages
	private Context context;
	private ProgressBar pb; //Progress dialog pour les temps de chargement
	private EditText inputAnswer;
	private Button validateAnswer;
	private TextView txtScore;
	private int score = 0;
	private int joker = MAX_JOKER;
	private JSONArray celeb;
	private int currentCeleb = 0;
	private final static int CHECK_TTS_SUBACTIVITY = 3;
	private TextToSpeech myTTS;
	
	private boolean isBlind = false;
	private boolean isDeaf = false;
	private boolean isVoiceInput = false;
	private boolean isVibrate = false;

	private SoundPool soundPool;
	private SparseIntArray soundsMap;
	private static int BOSSDEATH_SOUND = 1;
	private static int CANCEL_SOUND = 2;
	private static int CONFIRM_SOUND = 3;
	private static int MENU_SOUND = 4;
	private static int SELECT_SOUND = 5;
	private static int TEXTBLOOP_SOUND = 6;
	
	private Vibrator vibrator;
	private static long[] BOSSDEATH_PATTERN = {0, 300};
	private static long[] CANCEL_PATTERN = {0, 200, 100, 200};
	private static long[] CONFIRM_PATTERN = {0, 500};
	private static long[] MENU_PATTERN = {0, 500, 100, 500};
	private static long[] SELECT_PATTERN = {0, 100};
	private static long[] TEXTBLOOP_PATTERN = {0, 100, 100, 100, 100, 100};
	
	private boolean isLoaded = false;

	public int getScore(){
		return score;
	}
	public int getJoker(){
		return joker;
	}
	
	public boolean isLoaded() {
		return isLoaded();
	}
	
	public void setLoadStatus(boolean b) {
		this.isLoaded = b;
	}
	
	public void onInit(int initStatus) {
	    if (initStatus == TextToSpeech.SUCCESS) {
	        myTTS.setLanguage(Locale.US);
	    }
	}
	
	private void retrieveCelebrities() {
		// Ouverture et récupération du contenu du fichier json
		String jsonFile = null;
		try {
			InputStream is = getAssets().open("celebrities.json");
			int sizeBuf = is.available();
			byte[] buffer = new byte[sizeBuf];
			is.read(buffer);
			is.close();
			jsonFile = new String(buffer, "UTF-8");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Parsing du json et récupération des données dans un ArrayList
		JSONParser parser = new JSONParser();
		try {
			JSONObject jsonObject = (JSONObject)parser.parse(jsonFile);
			this.celeb = (JSONArray) jsonObject.get("celebrities");
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void initializeSoundPool() {
		// Objet qui joue les sons
		soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		// Objet équivalent à HashMap<Integer, Integer> mais plus rapide
		soundsMap = new SparseIntArray();
		soundsMap.put(BOSSDEATH_SOUND, soundPool.load(this, R.raw.bossdeath, 1));
		soundsMap.put(CANCEL_SOUND, soundPool.load(this, R.raw.cancel, 1));
		soundsMap.put(CONFIRM_SOUND, soundPool.load(this, R.raw.confirm, 1));
		soundsMap.put(MENU_SOUND, soundPool.load(this, R.raw.menu, 1));
		soundsMap.put(SELECT_SOUND, soundPool.load(this, R.raw.select, 1));
		soundsMap.put(TEXTBLOOP_SOUND, soundPool.load(this, R.raw.textboxbloop, 1));
	}
	
	private void playSound(int soundToPlay, int loop, float rate) {
		AudioManager mgr = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
		float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
		float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = streamVolumeCurrent / streamVolumeMax;
		soundPool.play(soundsMap.get(soundToPlay), volume, volume, 1, loop, rate);
	}
	
	private void initializePreferences() {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		isBlind = sharedPref.getBoolean(Configuration.MAL_VOYANT_PREF, false);
		isDeaf = sharedPref.getBoolean(Configuration.MAL_ENTENDANT_PREF, false);
		isVoiceInput = sharedPref.getBoolean(Configuration.VOICE_INPUT_PREF, false);
		isVibrate = sharedPref.getBoolean(Configuration.VIBREUR_PREF, false);
	}
	
	private void initializeVibrator() {
		vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
	}
	
	private void vibrate(long[] pattern) {
		vibrator.vibrate(pattern, -1);
	}
	
	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jeu);
		context = this;

		//Creation du progressDialog
		pb = (ProgressBar) findViewById(R.id.progressBar1);

		imageView = (ImageView) findViewById(R.id.quizzImage);
		inputAnswer = (EditText) findViewById(R.id.inputAnswer);
		txtScore = (TextView) findViewById(R.id.jeu_score);
		mToast = Toast.makeText(context, "", Toast.LENGTH_LONG);
		
		initializePreferences();
		retrieveCelebrities();
		initializeSoundPool();
		initializeVibrator();
		
		Intent checkTTSIntent = new Intent();
		checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkTTSIntent, CHECK_TTS_SUBACTIVITY);
		
		new SearchImages().execute((String)celeb.get(currentCeleb));
	}

	@Override
	protected void onResume() {
		super.onStart();

		//Utilisation d'un joker
		TextView jokerTxt = (TextView) findViewById(R.id.useJoker);
		jokerTxt.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isLoaded == false)
					return;
				indexImg++;
				if(joker > 0) {
					joker--;
					if (!isDeaf){
						playSound(SELECT_SOUND, 1, 1.0f);
					}
					if(isVibrate){
						vibrate(SELECT_PATTERN);
					}
					if(isBlind){
						myTTS.speak("You used a joker." +joker+" left.", TextToSpeech.QUEUE_FLUSH, null);
					}
					TextView jokerNb = (TextView) findViewById(R.id.nbJoker);
					jokerNb.setText("("+joker + " left)");	//on actualise le nombre de joker
					Photo photo = (Photo) photoListe.get(indexImg); //on récupere la nouvelle poto dans la liste
					BitmapWorkerTask task = new BitmapWorkerTask(imageView);
				    task.execute(photo);
				}
				else {
					if (!isDeaf){
						playSound(CANCEL_SOUND, 1, 1.0f);
					}
					if(isVibrate){
						vibrate(CANCEL_PATTERN);
					}
					if(isBlind){
						myTTS.speak("You don't have any joker left !", TextToSpeech.QUEUE_FLUSH, null);
					}
					else {
						mToast.setText("You don't have any joker left !");
						mToast.show();
					}
				}
			}
		});

		//Validation de la réponse
		validateAnswer = (Button) findViewById(R.id.validateAnswer);
		validateAnswer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				inputAnswer.clearFocus();	//remove le focus de l'edit text
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);	//crée l'input manager
				imm.hideSoftInputFromWindow(inputAnswer.getWindowToken(), 0);	//on cache le soft keyboard
				// TODO Auto-generated method stub
				String answer = inputAnswer.getText().toString();
				if(answer.equalsIgnoreCase((String)celeb.get(currentCeleb))){
					score++;	//on incrémente le score de 1.
					if (!isDeaf){
						playSound(CONFIRM_SOUND, 2, 1.0f);
					}
					if(isVibrate){
						vibrate(CONFIRM_PATTERN);
					}
					
					currentCeleb++;	//on change de celebrité
					inputAnswer.setText("");	//on vide l'input de réponse
					indexImg = 0;

					AlertDialog.Builder builder = new AlertDialog.Builder(context);
					if(currentCeleb < celeb.size()) {
						//S'il reste des celebrité, on continue le jeu avec de nouvelles images
						if(isBlind){
							myTTS.speak("Good answer ! Your have now "+score+" points.", TextToSpeech.QUEUE_FLUSH, null);
						}
						builder.setTitle("Good answer !")
						.setMessage("Your have now "+score+" points.")
						.setPositiveButton(R.string.okBtn, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								txtScore.setText(Integer.toString(score));	//on met a jour le score
								new SearchImages().execute((String)celeb.get(currentCeleb));	//on lance la nouvelle recherche
							}
						});
					}
					else {
						//S'il ne reste plus de celebrités, on a fini le jeu !
						if(isBlind){
							myTTS.speak("Good job ! You have finished the game with "+score+" points.", TextToSpeech.QUEUE_FLUSH, null);
						}
						builder.setTitle("Good Job !")
						.setMessage("You have finished the game with "+score+" points.")
						.setPositiveButton(R.string.okBtn, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								txtScore.setText(Integer.toString(score));	//on met a jour le score
								imageView.setImageResource(R.color.white);
							}
						});
					}
					AlertDialog dialog = builder.create();	//on crée l'alerte
					dialog.show();	//on fait apparaitre l'alerte
				}
				else {
					if (!isDeaf){
						playSound(BOSSDEATH_SOUND, 1, 1.0f);
					}
					if(isVibrate){
						vibrate(BOSSDEATH_PATTERN);
					}
					if(isBlind){
						myTTS.speak("Bad answer, try again !", TextToSpeech.QUEUE_FLUSH, null);
					}
					else{
						mToast.setText("Bad answer, try again !");
						mToast.show();
					}				
				}
			}
		});

		//BackButton
		ImageButton backButton = (ImageButton) findViewById(R.id.jeu_back);
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isDeaf){
					playSound(MENU_SOUND, 1, 1.0f);
				}
				if(isVibrate){
					vibrate(MENU_PATTERN);
				}
				if(isBlind){
					myTTS.speak("Going back to menu", TextToSpeech.QUEUE_FLUSH, null);
				}
				finish();
			}
		});
		
		
		// Voice input button
		Button voiceInputButton = (Button) findViewById(R.id.buttonVoiceInput);
		if (SpeechRecognizer.isRecognitionAvailable(this) == false || isVoiceInput == false) {	
			//voiceInputButton.setEnabled(false);
		}
		else {
			voiceInputButton.setVisibility(View.VISIBLE);
			voiceInputButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
					i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
					try {
						startActivityForResult(i, VOICE_INPUT_SUBACTIVITY);
					} catch (Exception e) {
						if (isBlind) {
							myTTS.speak("Initializing voice input error", TextToSpeech.QUEUE_FLUSH, null);
						}
						else {
							mToast.setText("Initializing voice input error !");
							mToast.show();
						}
						
					}				
				}
			});
		}
		
		ImageView image = (ImageView) findViewById(R.id.quizzImage);
		image.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {		
				if (isLoaded == false)
					return;
				ImageView img = (ImageView)v;
				Intent intent = new Intent(Jeu.this, AffichageImage.class);
				Bitmap bitmap = ((BitmapDrawable)img.getDrawable()).getBitmap();
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
		        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		        byte[] bytes = stream.toByteArray(); 
		        intent.putExtra("BMP",bytes);
				startActivity(intent);
			}
		});
	}
	
	protected void voiceCommand(Intent data) {
		ArrayList<String> thingsYouSaid = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
		String str = thingsYouSaid.get(0);
		Levenshtein l = new Levenshtein();
		float seuil = 0.8f;
		if (l.getSimilarity(str, "Effacer") > seuil) {
			if (!isDeaf){
				playSound(CANCEL_SOUND, 1, 1.0f);
			}
			if(isVibrate){
				vibrate(CANCEL_PATTERN);
			}
			if(isBlind){
				myTTS.speak("Erasing input", TextToSpeech.QUEUE_FLUSH, null);
			}
			((TextView)findViewById(R.id.inputAnswer)).setText("");
		}
		else if (l.getSimilarity(str, "Valider") > seuil) {
			Button buttonOk = (Button)findViewById(R.id.validateAnswer);
			buttonOk.performClick();
		}
		else if (l.getSimilarity(str, "Utiliser joker") > seuil) {
			TextView buttonJoker = (TextView)findViewById(R.id.useJoker);
			buttonJoker.performClick();
		}
		else if (l.getSimilarity(str, "Retour au menu") > seuil) {
			ImageButton buttonBack = (ImageButton)findViewById(R.id.jeu_back);
			buttonBack.performClick();
		}
		else if (l.getSimilarity(str, "Agrandir image") > seuil) {
			ImageView img = (ImageView)findViewById(R.id.quizzImage);
			img.performClick();
		}
		else {
			if (!isDeaf){
				playSound(TEXTBLOOP_SOUND, 1, 1.0f);
			}
			if(isVibrate){
				vibrate(TEXTBLOOP_PATTERN);
			}
			if(isBlind){
				myTTS.speak("The input is " + str, TextToSpeech.QUEUE_FLUSH, null);
			}
			((TextView)findViewById(R.id.inputAnswer)).setText(str);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == VOICE_INPUT_SUBACTIVITY  && resultCode == RESULT_OK) {
			voiceCommand(data);
		}
		if (requestCode == CHECK_TTS_SUBACTIVITY) {
		    if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {      
		            myTTS = new TextToSpeech(this, this);
		    }
		    else {
		            Intent installTTSIntent = new Intent();
		            installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
		            startActivity(installTTSIntent);
		    }
		}
	}
	
	

	//Asynctask de chargement d'images
	private class SearchImages extends AsyncTask<String, String, PhotoList>{

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			isLoaded = false;
			pb.setVisibility(View.VISIBLE);
		}

		@Override
		protected PhotoList doInBackground(String... params) {
			//Génération de l'interface flickr
			String apiKey = "1eeeb7201de55a11ca9f04ee4045f31d";
			String apiSecret = "3dba08bfdccb6eec";
			Flickr f = new Flickr(apiKey, apiSecret);

			 //Initialisation de l'objet paramètre de la recherche
			 SearchParameters searchParams = new SearchParameters();
			 searchParams.setSort(SearchParameters.INTERESTINGNESS_ASC);

			 //Mot-clé
			 String recherche = params[0];
			 searchParams.setText(recherche);

			 PhotosInterface photosInterface = f.getPhotosInterface();
			try {
				photoListe = photosInterface.search(searchParams, MAX_JOKER + 1, 1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FlickrException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return photoListe;
		}

		@Override
		protected void onPostExecute(PhotoList photoListe) {
			super.onPostExecute(photoListe);
			Photo photo = (Photo) photoListe.get(0); //on recupere le premier résultat
			BitmapWorkerTask task = new BitmapWorkerTask(imageView);	//on crée la tache chargé de créer la bitmap
		    task.execute(photo);	//on execute la tache en asynchrone
		}
	}

	//AsyncTask bitmap pour afficher l'image
	private class BitmapWorkerTask extends AsyncTask<Photo, Void, Bitmap> {
	    private final WeakReference<ImageView> imageViewReference;
	    private Photo data = null;

	    public BitmapWorkerTask(ImageView imageView) {
	        // Use a WeakReference to ensure the ImageView can be garbage collected
	        imageViewReference = new WeakReference<ImageView>(imageView);
	    }

	    @Override
	    protected void onPreExecute() {
	    	super.onPreExecute();
	    	isLoaded = false;
	    	imageView.setImageResource(R.color.white);	//on reinitialise la source de l'image
	    	pb.setVisibility(View.VISIBLE);	//on affiche la progressbar
	    }

	    // Decodage de l'image en background
	    @Override
	    protected Bitmap doInBackground(Photo... params) {
	        data = params[0];
	        Bitmap bitmap = null;
	        try {
	        	//URL url = new URL("http://farm"+data.getFarm()+".staticflickr.com/"+data.getServer()+"/"+data.getId()+"_"+data.getSecret()+".jpg");
	        	URL url = new URL(data.getMediumUrl());	//récupère l'url medium
	        	HttpURLConnection connection = (HttpURLConnection) url.openConnection();	//on ouvre la connexion http
	        	InputStream is = connection.getInputStream();	//création de l'inputstream
				bitmap = BitmapFactory.decodeStream(is);	//création de la bitmap
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return bitmap;
	    }

	    // Une fois terminé on regarde si l'imageViewexiste toujours et on set la bitmap
	    @Override
	    protected void onPostExecute(Bitmap bitmap) {
	        if (imageViewReference != null && bitmap != null) {
	            ImageView imageView = imageViewReference.get();
	            if (imageView != null) {
	                imageView.setImageBitmap(bitmap);	//on set le src de l'imageview avec la bitmap
	                pb.setVisibility(View.GONE);
	                isLoaded = true;
	            }
	        }
	    }
	}

}
